CREATE DATABASE `bank`;

use bank;
CREATE TABLE `client` (
  `idclient` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `cardNumber` varchar(45) NOT NULL,
  `CNP` varchar(45) NOT NULL,
  `adress` varchar(45) NOT NULL,
  PRIMARY KEY (`idclient`),
  UNIQUE KEY `idclient_UNIQUE` (`idclient`),
  UNIQUE KEY `cardNumber_UNIQUE` (`cardNumber`),
  UNIQUE KEY `CNP_UNIQUE` (`CNP`)
)  ;

CREATE TABLE `account` (
  `idaccount` int(11) NOT NULL AUTO_INCREMENT,
  `idclient` int(11) NOT NULL,
  `idNumber` varchar(45) NOT NULL,
  `type` varchar(45) NOT NULL,
  `money` double DEFAULT NULL,
  `datecreation` date NOT NULL,
  PRIMARY KEY (`idaccount`),
  UNIQUE KEY `idaccount_UNIQUE` (`idaccount`),
  UNIQUE KEY `idNumber_UNIQUE` (`idNumber`),
  KEY `idclient_idx` (`idclient`),
  CONSTRAINT `idclient` FOREIGN KEY (`idclient`) REFERENCES `client` (`idclient`) ON DELETE CASCADE ON UPDATE CASCADE
)  ;

CREATE TABLE `user` (
  `iduser` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `adress` varchar(45) NOT NULL,
  `salary` double NOT NULL,
  PRIMARY KEY (`iduser`),
  UNIQUE KEY `iduser_UNIQUE` (`iduser`)
);
  
  CREATE TABLE `bills` (
  `idbills` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `value` DOUBLE NOT NULL,
  `clientid` INT NOT NULL,
  `status` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idbills`),
  UNIQUE INDEX `idbills_UNIQUE` (`idbills` ASC),
  INDEX `idclient_idx` (`clientid` ASC),
  CONSTRAINT `clientid`
    FOREIGN KEY (`clientid`)
    REFERENCES `client` (`idclient`)
    ON DELETE CASCADE
    ON UPDATE CASCADE);


  INSERT INTO client ( name,cardNumber,CNP,adress) VALUES ( 'Rus Andrei','AX243654','1951113011341','str.Gheorghe Dima nr.17');				   
  INSERT INTO client ( name,cardNumber,CNP,adress) VALUES ( 'Rus Mihai','AX346654','1951113234341','str.Gheorghe Dima nr.17');
  INSERT INTO client ( name,cardNumber,CNP,adress) VALUES ( 'Rus Vasile','AX348954','1951121234341','str.Gheorghe Dima nr.20');

INSERT INTO account (idclient, idNumber,type,money,dateCreation)
                       VALUES
                       ('1','00001','Regular','250.0','2017-03-22');
INSERT INTO account (idclient, idNumber,type,money,dateCreation)
                       VALUES
                       ('2','00002','Regular','150.0','2017-03-23');
INSERT INTO account (idclient, idNumber,type,money,dateCreation)
                       VALUES
                       ('3','00003','Regular','157.0','2017-01-23');


INSERT INTO user ( name,password,adress,salary)
                       VALUES
                       ( 'asd','asd','str.Observatorului','1000');


INSERT INTO user ( name,password,adress,salary)
                       VALUES
                       ( 'Rus Andrei','parola','str.Observatorului','2500');

INSERT INTO user ( name,password,adress,salary)
                       VALUES
                       ( 'Rus Mihai','parola','str.Observatorului','1500');


INSERT INTO bills(name,value,clientid,status) VALUES ('Factura 1', 10, 2, 'Not paid');
INSERT INTO bills(name,value,clientid,status) VALUES ('Factura 2', 15, 3, 'Not paid');
INSERT INTO bills(name,value,clientid,status) VALUES ('Factura 3', 20, 3, 'Not paid');	
