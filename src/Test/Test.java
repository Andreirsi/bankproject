package Test;


import BusinessLayer.ValidationException;
import DataLayer.*;

import java.sql.Date;
import java.time.LocalDate;

import static junit.framework.TestCase.assertEquals;
import static junit.framework.TestCase.assertTrue;

public class Test {

   @org.junit.Test
    public void testAddClient() {
        Client c= new Client("florin", "1213","12314","lupsa");
        BankRepositoryImpl rep= new BankRepositoryImpl();
        try{
            rep.addClient(c);
            Client c2=rep.findbyCardNumber(c.getCardNumber());
            assertEquals(c.getCardNumber(),c2.getCardNumber());

        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    @org.junit.Test
    public void testUpdateClient() {
        Client c= new Client("florin1", "1214","121456","lupsa");
        BankRepositoryImpl rep= new BankRepositoryImpl();
        try{
            rep.addClient(c);
            Client c2=rep.findbyCardNumber("1214");
            Client c1= new Client("florina", "1211","121123","lupsa");
            c1.setIdclient(c2.getIdclient());
            rep.update(c1);
            c2=rep.findbyCardNumber("1211");
            assertEquals(c1.getName(),c2.getName());

        }
        catch (Exception e){
            e.printStackTrace();
         }
    }

    @org.junit.Test
    public void testAddAccount() throws ValidationException {

        BankRepositoryImpl rep= new BankRepositoryImpl();
        LocalDate localDate = LocalDate.now();
        Date date = java.sql.Date.valueOf(localDate);

        Account a= new Account(rep.findById(1),"0100","Regular",1500,date);
        try{
            rep.addAccount(a);
            Account a2=rep.findAccByIdNumber(a.getIdNumber());
            assertEquals(a.getIdNumber(),a2.getIdNumber());
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

@org.junit.Test
public void testAddUser() {

    BankRepositoryImpl rep= new BankRepositoryImpl();

    User u=new User("Andrei","parolaa","Blaj",1500);

    try{
        rep.addUser(u);
        User u2=rep.findUserbyName(u.getName());
        assertEquals(u.getName(),u2.getName());
    }
    catch (Exception e){
        e.printStackTrace();
    }
}


    @org.junit.Test
    public void testPayBill() {
        int idbill=3;

        BankRepositoryImpl rep= new BankRepositoryImpl();
        try{
            Bills bill=rep.findBillById(idbill);
            Account a=rep.findAccByClientId(bill.getClient().getIdclient());

            rep.payBill(bill,a, bill.getValue());
            bill=rep.findBillById(idbill);
            assertTrue(bill.getStatus().equals("PAID"));
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }


    @org.junit.Test
    public void testTransfer() {
        int source=2, destination=3;
        double amount=2;
        BankRepositoryImpl rep= new BankRepositoryImpl();
        try{
            Account a=rep.findAccById(source);
            Account a2=rep.findAccById(destination);
            double amountInit=a.getMoney();
            double amountInit2=a2.getMoney();
            rep.updateSuma(a,a2,amount);
            a=rep.findAccById(source);
            a2=rep.findAccById(destination);
            assertTrue(amountInit-amount==a.getMoney());
            assertTrue(amountInit2+amount==a2.getMoney());
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }


}
