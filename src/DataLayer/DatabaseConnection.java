package DataLayer;

import javax.swing.*;
import java.sql.*;
import java.util.Properties;


public class DatabaseConnection {


    public Connection connectToDatabaseAsClient() {
        Connection connection = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            String dbURL = "jdbc:mysql://localhost/bank";
            Properties properties = new Properties();
            properties.setProperty("user", "root");
            properties.setProperty("password", "root");
            properties.setProperty("useSSL", "false");
            properties.setProperty("noAccessToProcedureBodies","true");


            connection = DriverManager.getConnection(dbURL, properties);
        }
        catch (Exception exception) {
            JOptionPane.showMessageDialog(null, "Establising database connection failed", "Error", JOptionPane.ERROR_MESSAGE);
        }

        return connection;
    }

    public boolean connectToDatabaseAsSupplier(String name,String pass) {
        Connection connection = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            String dbURL = "jdbc:mysql://localhost/bank";
            Properties properties = new Properties();
            properties.setProperty("user", name);
            properties.setProperty("password", pass);
            properties.setProperty("useSSL", "false");
            properties.setProperty("noAccessToProcedureBodies","true");


            connection = DriverManager.getConnection(dbURL, properties);
        }
        catch (Exception exception) {

        }

        if(connection!=null){return true;}
        return false;
    }

    public boolean clientConnection(String user,String pass) throws SQLException {

        DatabaseConnection c=new DatabaseConnection();
        Connection connection= c.connectToDatabaseAsClient();


        Statement stmt = connection.createStatement();
        String SQL = "SELECT * FROM user WHERE name='" + user + "' && password='" + pass+ "'";

        ResultSet rs = stmt.executeQuery(SQL);

        if (rs.first() ) {
            return true;
        } else {
            return false;
        }
    }


    public void closeConnection(Connection con) throws SQLException {
        con.close();
    }



}