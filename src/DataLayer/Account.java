package DataLayer;

import java.sql.Date;


public class Account {
    private int idaccount;
    private Client client;
    private String idNumber;
    private String type;
    private double money;
    private Date dateCreation;

    public Account() {
    }

    public Account(Client client, String idNumber, String type, double money, Date dateCreation) {
        this.client = client;
        this.idNumber = idNumber;
        this.type = type;
        this.money = money;
        this.dateCreation = dateCreation;
    }

    public int getIdaccount() {
        return idaccount;
    }

    public void setIdaccount(int idaccount) {
        this.idaccount = idaccount;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public String getIdNumber() {
        return idNumber;
    }

    public void setIdNumber(String idNumber) {
        this.idNumber = idNumber;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public double getMoney() {
        return money;
    }

    public void setMoney(double money) {
        this.money = money;
    }

    public Date getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(Date dateCreation) {
        this.dateCreation = dateCreation;
    }
}
