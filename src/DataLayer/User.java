package DataLayer;


public class User {
    private int iduser;
    private String name;
    private String password;
    private String adress;
    private double salary;

    public User() {
    }

    public User(String name, String password, String adress, double salary) {
        this.name = name;
        this.password = password;
        this.adress = adress;
        this.salary = salary;
    }

    public int getIduser() {
        return iduser;
    }

    public void setIduser(int iduser) {
        this.iduser = iduser;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double salary) {
        this.salary = salary;
    }
}
