package DataLayer;


public class Client {
    private int idclient;
    private String name;
    private String cardNumber;
    private String CNP;
    private String adress;
public Client(){

}
    public Client(String name, String cardNumber, String cNP, String adress) {
        this.name=name;
        this.cardNumber=cardNumber;
        this.CNP=cNP;
        this.adress=adress;
    }

    public int getIdclient() {
        return idclient;
    }

    public void setIdclient(int idclient) {
        this.idclient = idclient;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getCNP() {
        return CNP;
    }

    public void setCNP(String CNP) {
        this.CNP = CNP;
    }

    public String getAdress() {
        return adress;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }
}
