package DataLayer;


public class Bills {
    private int idbills;
    private String name;
    private double value;
    private Client client;
    private String status;

    public int getIdbills() {
        return idbills;
    }

    public void setIdbills(int idbills) {
        this.idbills = idbills;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getValue() {
        return value;
    }

    public void setValue(double value) {
        this.value = value;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
