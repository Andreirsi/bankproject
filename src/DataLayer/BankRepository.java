package DataLayer;


import BusinessLayer.*;

import java.sql.ResultSet;

public interface BankRepository {

     boolean addClient(Client client) throws ValidationException;
     Client findById(int idclient) throws ValidationException;
     Client findbyCardNumber(String cardNumber) throws ValidationException;
     boolean update(Client client) throws ValidationException;
     ResultSet view() throws  ValidationException;
     boolean addAccount(Account account) throws ValidationException;
     Account findAccById(int idaccount) throws ValidationException;
     Account findAccByIdNumber(String idNumber) throws ValidationException;
     boolean updateAccount(Account account) throws ValidationException;
     ResultSet viewAccounts() throws  ValidationException;
     boolean deleteAccount(Account account) throws ValidationException;
     boolean addUser(User user) throws ValidationException;
     ResultSet viewUser() throws  ValidationException;
     boolean updateUser(User user) throws ValidationException;
     boolean deleteUser(User user) throws ValidationException;
     User findUserbyId(int iduser) throws ValidationException;
     User findUserbyName(String username) throws ValidationException;
     boolean updateSuma(Account a1,Account a2,double suma) throws ValidationException ;
    boolean payBill(Bills b, Account a, double suma) throws ValidationException;
    Bills findBillById(int idbill) throws ValidationException;
     Account findAccByClientId(int idaccount) throws ValidationException;
     ResultSet viewBills() throws  ValidationException;

}
