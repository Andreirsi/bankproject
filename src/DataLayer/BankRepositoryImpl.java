package DataLayer;

import BusinessLayer.*;

import java.sql.*;


public class BankRepositoryImpl implements BankRepository{

    @Override
    public boolean addClient(Client client) throws ValidationException {
        try {
            DatabaseConnection con=new DatabaseConnection();
            Connection db =	con.connectToDatabaseAsClient();

            String statement = "INSERT INTO `client` (`name`, `cardNumber`, `CNP`,`adress`) VALUES (?,?,?,?)";
            PreparedStatement dbStatement = db.prepareStatement(statement);
            dbStatement.setString(1, client.getName());
            dbStatement.setString(2, client.getCardNumber());
            dbStatement.setString(3, client.getCNP());
            dbStatement.setString(4, client.getAdress());
            dbStatement.executeUpdate();
            dbStatement.close();
            db.close();
        } catch (SQLException e) {
           e.printStackTrace();
           return false;
        }
        return true;
    }

    public Client findById(int idclient) throws ValidationException {
        try {
            Client c=new Client();
            DatabaseConnection con=new DatabaseConnection();
            Connection db =	con.connectToDatabaseAsClient();

            String statement = "SELECT `name`, `cardNumber`, `CNP`, `adress`,`idclient` FROM `client` where `idclient`=?";
            PreparedStatement dbStatement = db.prepareStatement(statement);
            dbStatement.setInt(1,idclient);
            ResultSet rs = dbStatement.executeQuery();

            while(rs.next()) {
                String name = rs.getString("name");
                String cardNumber = rs.getString("cardNumber");
                String CNP = rs.getString("CNP");
                String adress = rs.getString("adress");
                int idclient1 = rs.getInt("idclient");



                c.setName(name);
                c.setCardNumber(cardNumber);
                c.setCNP(CNP);
                c.setAdress(adress);
                c.setIdclient(idclient1);
                db.close();
                dbStatement.close();
                return c;
            }
            return null;

        } catch (SQLException e) {
          e.printStackTrace();
          return null;
        }
    }

    @Override
    public Client findbyCardNumber(String cardNumber) throws ValidationException {
        try {
            Client c=new Client();
            DatabaseConnection con=new DatabaseConnection();
            Connection db =	con.connectToDatabaseAsClient();

            String statement = "SELECT `name`, `cardNumber`, `CNP`, `adress`,`idclient` FROM `client` where `cardNumber`=?";
            PreparedStatement dbStatement = db.prepareStatement(statement);
            dbStatement.setString(1,cardNumber);
            ResultSet rs = dbStatement.executeQuery();

            while(rs.next()) {
                String name = rs.getString("name");
                String cardNumber1 = rs.getString("cardNumber");
                String CNP = rs.getString("CNP");
                String adress = rs.getString("adress");
                int idclient1 = rs.getInt("idclient");



                c.setName(name);
                c.setCardNumber(cardNumber1);
                c.setCNP(CNP);
                c.setAdress(adress);
                c.setIdclient(idclient1);
                db.close();
                dbStatement.close();
                return c;
            }
            return null;

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }


    public boolean update(Client client) throws ValidationException {
        try {
            DatabaseConnection con=new DatabaseConnection();
            Connection db =	con.connectToDatabaseAsClient();

            String statement =  "UPDATE `client` SET `name`=?, `cardNumber`=?, `CNP`=?, `adress`=?   where `idclient`=?";
            PreparedStatement dbStatement = db.prepareStatement(statement);
            dbStatement.setString(1, client.getName());
            dbStatement.setString(2,client.getCardNumber());
            dbStatement.setString(3,client.getCNP());
            dbStatement.setString(4,client.getAdress());
            dbStatement.setInt(5,client.getIdclient());
            dbStatement.executeUpdate();
            db.close();
            dbStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    public ResultSet view() throws  ValidationException{
        DatabaseConnection con=new DatabaseConnection();
        Connection db =	con.connectToDatabaseAsClient();

        String statement =  "SELECT * FROM client";
        PreparedStatement dbStatement;
        try {
            dbStatement = db.prepareStatement(statement);
            ResultSet rs=  dbStatement.executeQuery();

            return rs;
        } catch (SQLException e) {
            e.printStackTrace();
        }
      return null;
    }

    @Override
    public boolean addAccount(Account account) throws ValidationException {
       try {
            DatabaseConnection con=new DatabaseConnection();
            Connection db =	con.connectToDatabaseAsClient();

            String statement = "INSERT INTO `account` ( `idclient`, `idNumber`,`type`,`money`,`datecreation`) VALUES (?,?,?,?,?)";
            PreparedStatement dbStatement = db.prepareStatement(statement);
            dbStatement.setInt(1,account.getClient().getIdclient());
            dbStatement.setString(2, account.getIdNumber());
            dbStatement.setString(3, account.getType());
            dbStatement.setDouble(4, account.getMoney());
            dbStatement.setDate(5, account.getDateCreation());
            dbStatement.executeUpdate();
            dbStatement.close();
            db.close();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public Account findAccById(int idaccount) throws ValidationException {
        try {
            Account c=new Account();
            DatabaseConnection con=new DatabaseConnection();
            Connection db =	con.connectToDatabaseAsClient();

            String statement = "SELECT `idaccount`, `idclient`, `idNumber`, `type`,`money`,`datecreation` FROM `account` where `idaccount`=?";
            PreparedStatement dbStatement = db.prepareStatement(statement);
            dbStatement.setInt(1,idaccount);
            ResultSet rs = dbStatement.executeQuery();

            while(rs.next()) {
                int idaccount1=rs.getInt("idaccount");
                int idclient=rs.getInt("idclient");
                String idNumber = rs.getString("idNumber");
                String type = rs.getString("type");
                double money = rs.getDouble("money");
                Date date = rs.getDate("datecreation");




                c.setIdaccount(idaccount1);
                c.setClient(findById(idclient));
                c.setIdNumber(idNumber);
                c.setType(type);
                c.setMoney(money);
                c.setDateCreation(date);
                db.close();
                dbStatement.close();
                return c;
            }
            return null;

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Account findAccByIdNumber(String idNumber) throws ValidationException {
        try {
            Account c=new Account();
            DatabaseConnection con=new DatabaseConnection();
            Connection db =	con.connectToDatabaseAsClient();

            String statement = "SELECT `idaccount`, `idclient`, `idNumber`, `type`,`money`,`datecreation` FROM `account` where `idNumber`=?";
            PreparedStatement dbStatement = db.prepareStatement(statement);
            dbStatement.setString(1,idNumber);
            ResultSet rs = dbStatement.executeQuery();

            while(rs.next()) {
                int idaccount1=rs.getInt("idaccount");
                int idclient=rs.getInt("idclient");
                String idNumber2 = rs.getString("idNumber");
                String type = rs.getString("type");
                double money = rs.getDouble("money");
                Date date = rs.getDate("datecreation");




                c.setIdaccount(idaccount1);
                c.setClient(findById(idclient));
                c.setIdNumber(idNumber2);
                c.setType(type);
                c.setMoney(money);
                c.setDateCreation(date);
                db.close();
                dbStatement.close();
                return c;
            }
            return null;

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean updateAccount(Account account) throws ValidationException {
        try {
            DatabaseConnection con=new DatabaseConnection();
            Connection db =	con.connectToDatabaseAsClient();

            String statement =  "UPDATE `account` SET `idNumber`=?, `type`=?, `money`=? where `idaccount`=?";
            PreparedStatement dbStatement = db.prepareStatement(statement);
            dbStatement.setString(1, account.getIdNumber());
            dbStatement.setString(2,account.getType());
            dbStatement.setDouble(3,account.getMoney());
            dbStatement.setInt(4,account.getIdaccount());
            dbStatement.executeUpdate();
            db.close();
            dbStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public ResultSet viewAccounts() throws ValidationException {

        DatabaseConnection con=new DatabaseConnection();
        Connection db =	con.connectToDatabaseAsClient();

        String statement =  "SELECT * FROM account";
        PreparedStatement dbStatement;
        try {
            dbStatement = db.prepareStatement(statement);

            ResultSet rs=  dbStatement.executeQuery();


            return rs;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean deleteAccount(Account account) throws ValidationException {
        DatabaseConnection con = new DatabaseConnection();
        Connection db = con.connectToDatabaseAsClient();

        String statement = "DELETE FROM `account` where `idaccount`=?";
        PreparedStatement dbStatement = null;
        try {
            dbStatement = db.prepareStatement(statement);
            dbStatement.setInt(1, account.getIdaccount());
            dbStatement.executeUpdate();
        } catch (SQLException e1) {
            e1.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public boolean addUser(User user) throws ValidationException {
        try {
            DatabaseConnection con=new DatabaseConnection();
            Connection db =	con.connectToDatabaseAsClient();

            String statement = "INSERT INTO `user` ( `name`, `password`,`adress`,`salary`) VALUES (?,?,?,?)";
            PreparedStatement dbStatement = db.prepareStatement(statement);
            dbStatement.setString(1,user.getName());
            dbStatement.setString(2, user.getPassword());
            dbStatement.setString(3, user.getAdress());
            dbStatement.setDouble(4, user.getSalary());
            dbStatement.executeUpdate();
            dbStatement.close();
            db.close();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public ResultSet viewUser() throws ValidationException {
        DatabaseConnection con=new DatabaseConnection();
        Connection db =	con.connectToDatabaseAsClient();

        String statement =  "SELECT * FROM user";
        PreparedStatement dbStatement;
        try {
            dbStatement = db.prepareStatement(statement);

            ResultSet rs=  dbStatement.executeQuery();


            return rs;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public boolean updateUser(User user) throws ValidationException {
        try {
            DatabaseConnection con=new DatabaseConnection();
            Connection db =	con.connectToDatabaseAsClient();

            String statement =  "UPDATE `user` SET `name`=?, `password`=?, `adress`=?, `salary`=? where `iduser`=?";
            PreparedStatement dbStatement = db.prepareStatement(statement);
            dbStatement.setString(1, user.getName());
            dbStatement.setString(2,user.getPassword());
            dbStatement.setString(3,user.getAdress());
            dbStatement.setDouble(4,user.getSalary());
            dbStatement.setInt(5,user.getIduser());
            dbStatement.executeUpdate();
            db.close();
            dbStatement.close();
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public boolean deleteUser(User user) throws ValidationException {
        DatabaseConnection con = new DatabaseConnection();
        Connection db = con.connectToDatabaseAsClient();

        String statement = "DELETE FROM `user` where `iduser`=?";
        PreparedStatement dbStatement = null;
        try {
            dbStatement = db.prepareStatement(statement);
            dbStatement.setInt(1, user.getIduser());
            dbStatement.executeUpdate();
        } catch (SQLException e1) {
            e1.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public User findUserbyId(int iduser) throws ValidationException {
        try {
            User c=new User();
            DatabaseConnection con=new DatabaseConnection();
            Connection db =	con.connectToDatabaseAsClient();

            String statement = "SELECT `iduser`, `name`, `password`, `adress`,`salary` FROM `user` where `iduser`=?";
            PreparedStatement dbStatement = db.prepareStatement(statement);
            dbStatement.setInt(1,iduser);
            ResultSet rs = dbStatement.executeQuery();

            while(rs.next()) {
                int iduser1=rs.getInt("iduser");
                String name = rs.getString("name");
                String password = rs.getString("password");
                String adress = rs.getString("adress");
                double salary= rs.getDouble("salary");
                c.setIduser(iduser1);
                c.setName(name);
                c.setPassword(password);
                c.setAdress(adress);
                c.setSalary(salary);
                db.close();
                dbStatement.close();
                return c;
            }
            return null;

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public User findUserbyName(String username) throws ValidationException {
        try {
            User c=new User();
            DatabaseConnection con=new DatabaseConnection();
            Connection db =	con.connectToDatabaseAsClient();

            String statement = "SELECT `iduser`, `name`, `password`, `adress`,`salary` FROM `user` where `name`=?";
            PreparedStatement dbStatement = db.prepareStatement(statement);
            dbStatement.setString(1,username);
            ResultSet rs = dbStatement.executeQuery();

            while(rs.next()) {
                int iduser1=rs.getInt("iduser");
                String name = rs.getString("name");
                String password = rs.getString("password");
                String adress = rs.getString("adress");
                double salary= rs.getDouble("salary");
                c.setIduser(iduser1);
                c.setName(name);
                c.setPassword(password);
                c.setAdress(adress);
                c.setSalary(salary);
                db.close();
                dbStatement.close();
                return c;
            }
            return null;

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public boolean updateSuma(Account a1, Account a2, double suma) throws ValidationException {
        try {
            DatabaseConnection con=new DatabaseConnection();
            Connection db =	con.connectToDatabaseAsClient();

            String statement =  "UPDATE `account` SET `money`=? where `idaccount`=?";
            String statement2 =  "UPDATE `account` SET `money`=? where `idaccount`=?";
            PreparedStatement dbStatement = db.prepareStatement(statement);
            PreparedStatement dbStatement2 = db.prepareStatement(statement2);
            dbStatement.setDouble(1, a1.getMoney()-suma);
            dbStatement.setInt(2,a1.getIdaccount());
            dbStatement.executeUpdate();
            dbStatement2.setDouble(1, a2.getMoney()+suma);
            dbStatement2.setInt(2,a2.getIdaccount());
            dbStatement2.executeUpdate();
            db.close();
            dbStatement.close();
            dbStatement2.close();

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public boolean payBill(Bills b, Account c, double suma) throws ValidationException {
        try {
            DatabaseConnection con=new DatabaseConnection();
            Connection db =	con.connectToDatabaseAsClient();

            String statement =  "UPDATE `account` SET `money`=? where `idaccount`=?";
            String statement2 =  "UPDATE `bills` SET `status`=? where `idbills`=?";
            PreparedStatement dbStatement = db.prepareStatement(statement);
            PreparedStatement dbStatement2 = db.prepareStatement(statement2);
            dbStatement.setDouble(1, (c.getMoney()-suma));
            dbStatement.setInt(2,c.getIdaccount());
            dbStatement.executeUpdate();
            dbStatement2.setString(1,"PAID");
            dbStatement2.setInt(2,b.getIdbills());
            dbStatement2.executeUpdate();
            db.close();
            dbStatement.close();
            dbStatement2.close();

        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    @Override
    public Bills findBillById(int idbill) throws ValidationException {
        try {
            Bills c=new Bills();
            DatabaseConnection con=new DatabaseConnection();
            Connection db =	con.connectToDatabaseAsClient();

            String statement = "SELECT `idbills`, `name`, `value`, `clientid`,`status` FROM `bills` where `idbills`=?";
            PreparedStatement dbStatement = db.prepareStatement(statement);
            dbStatement.setInt(1,idbill);
            ResultSet rs = dbStatement.executeQuery();

            while(rs.next()) {
                int iduser1=rs.getInt("idbills");
                int iduser2=rs.getInt("clientid");
                String name = rs.getString("name");
                double salary= rs.getDouble("value");
                String status = rs.getString("status");
                c.setIdbills(iduser1);
                c.setName(name);
                c.setClient(findById(iduser2));
                c.setValue(salary);
                c.setStatus(status);
                db.close();
                dbStatement.close();
                return c;
            }
            return null;

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public Account findAccByClientId(int idaccount) throws ValidationException {
        try {
            Account c=new Account();
            DatabaseConnection con=new DatabaseConnection();
            Connection db =	con.connectToDatabaseAsClient();

            String statement = "SELECT `idaccount`, `idclient`, `idNumber`, `type`,`money`,`datecreation` FROM `account` where `idclient`=?";
            PreparedStatement dbStatement = db.prepareStatement(statement);
            dbStatement.setInt(1,idaccount);
            ResultSet rs = dbStatement.executeQuery();

            while(rs.next()) {
                int idaccount1=rs.getInt("idaccount");
                int idclient=rs.getInt("idclient");
                String idNumber = rs.getString("idNumber");
                String type = rs.getString("type");
                double money = rs.getDouble("money");
                Date date = rs.getDate("datecreation");




                c.setIdaccount(idaccount1);
                c.setClient(findById(idclient));
                c.setIdNumber(idNumber);
                c.setType(type);
                c.setMoney(money);
                c.setDateCreation(date);
                db.close();
                dbStatement.close();
                return c;
            }
            return null;

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Override
    public ResultSet viewBills() throws ValidationException {
        DatabaseConnection con=new DatabaseConnection();
        Connection db =	con.connectToDatabaseAsClient();

        String statement =  "SELECT * FROM bills";
        PreparedStatement dbStatement;
        try {
            dbStatement = db.prepareStatement(statement);

            ResultSet rs=  dbStatement.executeQuery();


            return rs;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }


}
