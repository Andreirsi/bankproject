package BusinessLayer;

import DataLayer.Account;
import DataLayer.Bills;

import java.util.ArrayList;
import java.util.List;

public class ValidationImpl {

    public boolean validateClient(String name,String cardNumber, String cnp, String address) throws ValidationException {
        List<String> errors = new ArrayList<>();

        if (name.length()<1 ) {
            errors.add("name is null");
        }

        if (cardNumber.length()<1) {
            errors.add("cnp is null");
        }

        if (cnp.length()<1) {
            errors.add("cnp is null");
        }

        if (address.length()<1) {
            errors.add("address is null");
        }

        if (errors.size() > 0) {
            throw new ValidationException("Validation errors", errors);


        }
        return true;
    }

    public boolean validateUser(String name,String password, String adress, double salary) throws ValidationException {
        List<String> errors = new ArrayList<>();

        if (name.length()<1 ) {
            errors.add("name is null");
        }

        if (password.length()<1) {
            errors.add("password is null");
        }

        if (adress.length()<1) {
            errors.add("adress is null");
        }

        if (salary<0) {
            errors.add("salary is negative");
        }

        if (errors.size() > 0) {
            throw new ValidationException("Validation errors", errors);

        }
        return true;
    }
    public boolean validareAccount(String idNumber,String type, double doublee) throws ValidationException {
        List<String> errors = new ArrayList<>();

        if (idNumber.length()<1 ) {
            errors.add("number is null");
        }

        if (type.length()<1) {
            errors.add("type is null");
        }

        if (doublee <0 ) {
            errors.add("valoare incorecta");
        }


        if (errors.size() > 0) {
            throw new ValidationException("Validation errors", errors);

        }
        return true;
    }

public boolean validareTranzactie(Account a1, double suma) throws ValidationException{
    List<String> errors = new ArrayList<>();

    if ((a1.getMoney()-suma)<0 ) {
        errors.add("invalid tranzaction");
    }

    if (errors.size() > 0) {
        throw new ValidationException("Validation errors", errors);

    }
    return true;
}

    public boolean validarePayment(Bills b) throws ValidationException{
        List<String> errors = new ArrayList<>();

        if (b.getStatus().equals("PAID"))  {
            errors.add("bill already paid");
        }

        if (errors.size() > 0) {
            throw new ValidationException("Validation errors", errors);

        }
        return true;
    }

    public boolean validateIdClient(int idclient) throws ValidationException{
        List<String > errors=new ArrayList<>();
        if (idclient<0){
            errors.add("negative id");
        }
        if (errors.size()>0){
            throw new ValidationException("Validation errors", errors);
        }
        return true;
    }

    public boolean validareIdAccount(int idaccount) throws ValidationException{
        List<String > errors=new ArrayList<>();
        if (idaccount<0){
            errors.add("negative id");
        }
        if (errors.size()>0){
            throw new ValidationException("Validation errors", errors);
        }
        return true;
    }

    public boolean validareIdUser(int iduser) throws ValidationException{
        List<String > errors=new ArrayList<>();
        if (iduser<0){
            errors.add("negative id");
        }
        if (errors.size()>0){
            throw new ValidationException("Validation errors", errors);
        }
        return true;
    }

}
