package BusinessLayer;

import DataLayer.Account;
import DataLayer.Bills;
import DataLayer.Client;
import DataLayer.User;

import java.sql.ResultSet;


public class ValidationService implements  BankService{
    private ValidationImpl validation;
    private BankService origin;

    public ValidationService (ValidationImpl validation, BankService origin) {
        this.validation = validation;
        this.origin = origin;
    }

    @Override
    public boolean addClient(Client newClient) throws ValidationException {
    validation.validateClient(newClient.getName(),newClient.getCardNumber(),newClient.getCNP(),newClient.getAdress());
        return origin.addClient(newClient);

    }

    @Override
    public boolean update(Client client) throws ValidationException {
    validation.validateClient(client.getName(),client.getCardNumber(),client.getCNP(),client.getAdress());
        return origin.update(client);
    }

    @Override
    public Client findById(int idclient) throws ValidationException {
        validation.validateIdClient(idclient);
        return origin.findById(idclient);

    }

    @Override
    public ResultSet view() throws ValidationException {
        return origin.view();
    }

    @Override
    public boolean addAccount(Account account) throws ValidationException {
      validation.validareAccount(account.getIdNumber(),account.getType(),account.getMoney());
         return origin.addAccount(account);

    }

    @Override
    public Account findAccById(int idaccount) throws ValidationException {
  validation.validareIdAccount(idaccount);
        return origin.findAccById(idaccount);

    }

    @Override
    public boolean updateAccount(Account account) throws ValidationException {
        validation.validareAccount(account.getIdNumber(),account.getType(),account.getMoney());
            return origin.updateAccount(account);

    }

    @Override
    public ResultSet viewAccounts() throws ValidationException {
        return origin.viewAccounts();
    }

    @Override
    public boolean deleteAccount(Account account) throws ValidationException {
        return origin.deleteAccount(account);
    }

    @Override
    public boolean addUser(User user) throws ValidationException {
        validation.validateUser(user.getName(),user.getPassword(),user.getAdress(),user.getSalary());
        return origin.addUser(user);

    }

    @Override
    public ResultSet viewUser() throws ValidationException {
       return origin.viewUser();
    }

    @Override
    public boolean updateUser(User user) throws ValidationException {
        validation.validateUser(user.getName(), user.getPassword(), user.getAdress(), user.getSalary());
        return origin.updateUser(user);
    }

    @Override
    public boolean deleteUser(User user) throws ValidationException {
       return origin.deleteUser(user);
    }

    @Override
    public User findUserbyId(int iduser) throws ValidationException {
        validation.validareIdUser(iduser);
        return origin.findUserbyId(iduser);

    }

    @Override
    public boolean updateSuma(Account a1, Account a2, double suma) throws ValidationException {
      validation.validareTranzactie(a1,suma);
        return origin.updateSuma(a1,a2,suma);

    }

    @Override
    public boolean payBill(Bills b, Account a, double suma) throws ValidationException {
        validation.validarePayment(b);
        return origin.payBill(b,a,suma);
    }

    @Override
    public Bills findBillById(int idbill) throws ValidationException {
        return origin.findBillById(idbill);
    }

    @Override
    public Account findAccByClientId(int idaccount) throws ValidationException {
        return origin.findAccByClientId(idaccount);
    }

    @Override
    public ResultSet viewBills() throws ValidationException {
        return origin.viewBills();
    }

}
