package BusinessLayer;

import DataLayer.Account;
import DataLayer.Bills;
import DataLayer.Client;
import DataLayer.User;

import java.sql.ResultSet;


public interface BankService {
     boolean addClient(Client newClient) throws ValidationException;
     boolean update(Client client) throws ValidationException;
     Client findById(int idclient) throws ValidationException;
     ResultSet view() throws  ValidationException;
     boolean addAccount(Account account) throws ValidationException;
     Account findAccById(int idaccount) throws ValidationException;
     boolean updateAccount(Account account) throws ValidationException;
     ResultSet viewAccounts() throws  ValidationException;
     boolean deleteAccount(Account account) throws ValidationException;
     boolean addUser(User user) throws ValidationException;
     ResultSet viewUser() throws  ValidationException;
     boolean updateUser(User user) throws ValidationException;
     boolean deleteUser(User user) throws ValidationException;
     User findUserbyId(int iduser) throws ValidationException;
     boolean updateSuma(Account a1,Account a2,double suma) throws ValidationException;
     boolean payBill(Bills b, Account a, double suma) throws ValidationException;
     Bills findBillById(int idbill) throws ValidationException;
     Account findAccByClientId(int idaccount) throws ValidationException;
     ResultSet viewBills() throws  ValidationException;
}
