package BusinessLayer;


import DataLayer.*;

import java.sql.ResultSet;

public class BankRepositoryService implements BankService{

    private BankRepository repository;

    public BankRepositoryService(BankRepository repository){this.repository=repository;}

    @Override
    public boolean addClient(Client newClient) throws ValidationException {
        if(repository.addClient(newClient)){
            return true;
        }
        return false;
    }

    @Override
    public boolean update(Client client) throws ValidationException {
        if(repository.update(client)){
            return true;
        }
        return false;
    }

    @Override
    public Client findById(int idclient) throws ValidationException {
        if(repository.findById(idclient)!=null){
            return repository.findById(idclient);
        }
        return null;
    }

    @Override
    public ResultSet view() throws ValidationException {
        if(repository.view()!=null){
            return repository.view();
        }
        return null;
    }

    @Override
    public boolean addAccount(Account account) throws ValidationException {
        if(repository.addAccount(account)){
            return true;
        }
        return false;
    }

    @Override
    public Account findAccById(int idaccount) throws ValidationException {
        if(repository.findAccById(idaccount)!=null){
            return repository.findAccById(idaccount);
        }
        return null;
    }

    @Override
    public boolean updateAccount(Account account) throws ValidationException {
        if(repository.updateAccount(account)){
            return true;
        }
        return false;
    }

    @Override
    public ResultSet viewAccounts() throws ValidationException {
        if(repository.viewAccounts()!=null){
            return repository.viewAccounts();
        }
        return null;
    }

    @Override
    public boolean deleteAccount(Account account) throws ValidationException {
        if(repository.deleteAccount(account)){
            return true;
        }
        return false;
    }

    @Override
    public boolean addUser(User user) throws ValidationException {
        if(repository.addUser(user)){
            return true;
        }
        return false;
    }

    @Override
    public ResultSet viewUser() throws ValidationException {
        if(repository.viewUser()!=null){
            return repository.viewUser();
        }
        return null;
    }

    @Override
    public boolean updateUser(User user) throws ValidationException {
        if(repository.updateUser(user)){
            return true;
        }
        return false;
    }

    @Override
    public boolean deleteUser(User user) throws ValidationException {
        if(repository.deleteUser(user)){
            return true;
        }
        return false;
    }

    @Override
    public User findUserbyId(int iduser) throws ValidationException {
        if(repository.findUserbyId(iduser)!=null){
            return repository.findUserbyId(iduser);
        }
        return null;
    }

    @Override
    public boolean updateSuma(Account a1, Account a2, double suma) throws ValidationException  {
        if(repository.updateSuma(a1,a2,suma)){
            return true;
        }
        return false;
    }

    @Override
    public boolean payBill(Bills b, Account a, double suma) throws ValidationException {
        if(repository.payBill(b,a,suma)){
            return true;
        }
        return false;
    }

    @Override
    public Bills findBillById(int idbill) throws ValidationException {
        if(repository.findBillById(idbill)!=null){
            return repository.findBillById(idbill);
        }
        return null;
    }

    @Override
    public Account findAccByClientId(int idaccount) throws ValidationException {
        if(repository.findAccByClientId(idaccount)!=null){
            return repository.findAccByClientId(idaccount);
        }
        return null;
    }

    @Override
    public ResultSet viewBills() throws ValidationException {
        if(repository.viewBills()!=null){
            return repository.viewBills();
        }
        return null;
    }

}
