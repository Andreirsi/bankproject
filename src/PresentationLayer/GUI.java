package PresentationLayer;

import BusinessLayer.*;
import DataLayer.*;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;


public class GUI extends JFrame implements ActionListener {
    private ValidationImpl valimpl = new ValidationImpl();
    private BankService service = new BankRepositoryService(new BankRepositoryImpl());
    private BankService s = new ValidationService(new ValidationImpl(), service);
    private JButton connectAsUser;
    private JButton connectAsAdministrator;
    private DatabaseConnection dbcon = new DatabaseConnection();
    private JPanel mainPanel;
    boolean connectedAsSupplier = false;
    boolean connectedAsUser = false;
    private JFrame mainFrame = new JFrame("Log In Panel");
    private JDialog frame;


    LocalDate localDate = LocalDate.now();

    public GUI() {
        Date date = java.sql.Date.valueOf(localDate);
        JPanel panel1 = new JPanel();
        JPanel p = new JPanel();
        connectAsUser = new JButton("Connect as user");
        connectAsAdministrator = new JButton("Connect as administrator");
        panel1.add(connectAsUser);
        panel1.add(connectAsAdministrator);

        p.add(panel1);
        p.setLayout(new BoxLayout(p, BoxLayout.Y_AXIS));

        mainPanel = new JPanel();
        mainPanel.setLayout(new GridLayout(1, 1));
        mainPanel.setBackground(Color.WHITE);
        mainPanel.add(p);
        mainFrame.add(mainPanel);
        mainFrame.setSize(400, 80);
        mainFrame.setResizable(false);
        mainFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        mainFrame.setLocationRelativeTo(null);
        mainFrame.setVisible(true);

        connectAsUser.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JFrame x = new JFrame();
                JPanel a = new JPanel();
                JLabel label1 = new JLabel("User");

                JTextField area5 = new JTextField();
                area5.setPreferredSize(new Dimension(80, 24));
                a.add(label1);
                a.add(area5);
                a.setLayout(new FlowLayout());
                JPasswordField area6 = new JPasswordField();
                area6.setPreferredSize(new Dimension(80, 24));
                area6.setEchoChar('*');
                JLabel label2 = new JLabel("Password");

                a.add(label2);
                a.add(area6);
                JButton addClient = new JButton("OK");
                JButton deleteClient = new JButton("Cancel");
                a.setLayout(new FlowLayout());
                a.add(addClient);
                a.add(deleteClient);
                x.add(a);
                x.setSize(300, 300);
                x.setResizable(false);
                x.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                x.setLocationRelativeTo(null);
                x.setVisible(true);
                addClient.addActionListener(new java.awt.event.ActionListener() {
                                                @Override
                                                public void actionPerformed(java.awt.event.ActionEvent evt) {
                                                    try {

                                                        try {
                                                            connectedAsUser = dbcon.clientConnection(area5.getText(), area6.getText());
                                                        } catch (SQLException e) {
                                                            e.printStackTrace();
                                                        }
                                                        if (connectedAsUser) {
                                                            x.setVisible(false);
                                                            mainFrame.setVisible(false);
                                                            JFrame aaa = new JFrame();
                                                            aaa.setSize(500, 500);
                                                            aaa.setResizable(false);
                                                            aaa.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                                                            aaa.setLocationRelativeTo(null);
                                                            aaa.setVisible(true);
                                                            JPanel p1 = new JPanel();
                                                            JButton b1 = new JButton("Add Client");
                                                            JButton b2 = new JButton("Update Client");
                                                            JButton b3 = new JButton("View Clients");
                                                            JButton b4 = new JButton("Add Client Account");
                                                            JButton b5 = new JButton("Update Client Account");
                                                            JButton b6 = new JButton("Delete Client Account");
                                                            JButton b7 = new JButton("View Clients Accounts");
                                                            JButton b8 = new JButton("Transfer Money between accounts");
                                                            JButton b9 = new JButton("View Bills");
                                                            JButton b10 = new JButton("Pay Bills");

                                                            p1.add(b1);
                                                            p1.add(b2);
                                                            p1.add(b3);
                                                            p1.add(b4);
                                                            p1.add(b5);
                                                            p1.add(b6);
                                                            p1.add(b7);
                                                            p1.add(b8);
                                                            p1.add(b9);
                                                            p1.add(b10);
                                                            aaa.add(p1);
                                                            b1.addActionListener(new java.awt.event.ActionListener() {
                                                                                     @Override
                                                                                     public void actionPerformed(java.awt.event.ActionEvent evt) {
                                                                                         JFrame f1 = new JFrame();
                                                                                         f1.setSize(500, 500);
                                                                                         f1.setResizable(false);
                                                                                         f1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                                                                                         f1.setLocationRelativeTo(null);
                                                                                         f1.setVisible(true);
                                                                                         JPanel p1 = new JPanel();
                                                                                         JLabel l1 = new JLabel("nume: ");
                                                                                         JLabel l2 = new JLabel("card number: ");
                                                                                         JLabel l3 = new JLabel("cnp: ");
                                                                                         JLabel l4 = new JLabel("adress: ");
                                                                                         JTextField tf1 = new JTextField();
                                                                                         JTextField tf2 = new JTextField();
                                                                                         JTextField tf3 = new JTextField();
                                                                                         JTextField tf4 = new JTextField();
                                                                                         tf1.setPreferredSize(new Dimension(80, 24));
                                                                                         tf2.setPreferredSize(new Dimension(80, 24));
                                                                                         tf3.setPreferredSize(new Dimension(80, 24));
                                                                                         tf4.setPreferredSize(new Dimension(80, 24));
                                                                                         JButton add = new JButton("add");
                                                                                         JButton cancel = new JButton("cancel");
                                                                                         p1.add(l1);
                                                                                         p1.add(tf1);
                                                                                         p1.add(l2);
                                                                                         p1.add(tf2);
                                                                                         p1.add(l3);
                                                                                         p1.add(tf3);
                                                                                         p1.add(l4);
                                                                                         p1.add(tf4);
                                                                                         p1.add(add);
                                                                                         p1.add(cancel);
                                                                                         f1.add(p1);
                                                                                         add.addActionListener(new java.awt.event.ActionListener() {
                                                                                                                   @Override
                                                                                                                   public void actionPerformed(java.awt.event.ActionEvent evt) {
                                                                                                                       Client client = new Client();
                                                                                                                       client.setName(tf1.getText());
                                                                                                                       client.setCardNumber(tf2.getText());
                                                                                                                       client.setCNP(tf3.getText());
                                                                                                                       client.setAdress(tf4.getText());
                                                                                                                       try {
                                                                                                                           if (valimpl.validateClient(client.getName(), client.getCardNumber(), client.getCNP(), client.getAdress())) {
                                                                                                                               s.addClient(client);
                                                                                                                               f1.setVisible(false);
                                                                                                                           } else {
                                                                                                                               JOptionPane.showMessageDialog(frame,
                                                                                                                                       "Complete all fields.",
                                                                                                                                       "ERROR",
                                                                                                                                       JOptionPane.ERROR_MESSAGE);
                                                                                                                           }
                                                                                                                       } catch (Exception e) {
                                                                                                                           e.printStackTrace();
                                                                                                                       }
                                                                                                                   }
                                                                                                               }
                                                                                         );
                                                                                         cancel.addActionListener(new java.awt.event.ActionListener() {
                                                                                                                      @Override
                                                                                                                      public void actionPerformed(java.awt.event.ActionEvent evt) {
                                                                                                                          f1.setVisible(false);
                                                                                                                      }
                                                                                                                  }
                                                                                         );
                                                                                     }
                                                                                 }
                                                            );

                                                            b2.addActionListener(new java.awt.event.ActionListener() {
                                                                                     @Override
                                                                                     public void actionPerformed(java.awt.event.ActionEvent evt) {
                                                                                         String sidclient = JOptionPane.showInputDialog("Introduce-ti id-ul clientului pe care doriti sa il updatati ", "");
                                                                                         int idclient;
                                                                                         idclient = Integer.parseInt(sidclient);
                                                                                         try {
                                                                                             Client client = s.findById(idclient);

                                                                                             JFrame f1 = new JFrame();
                                                                                             f1.setSize(500, 500);
                                                                                             f1.setResizable(false);
                                                                                             f1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                                                                                             f1.setLocationRelativeTo(null);
                                                                                             f1.setVisible(true);
                                                                                             JPanel p1 = new JPanel();

                                                                                             JLabel l1 = new JLabel("nume: ");
                                                                                             JLabel l2 = new JLabel("card number: ");
                                                                                             JLabel l3 = new JLabel("cnp: ");
                                                                                             JLabel l4 = new JLabel("adress: ");
                                                                                             JTextField tf1 = new JTextField(client.getName());

                                                                                             JTextField tf2 = new JTextField(client.getCardNumber());
                                                                                             JTextField tf3 = new JTextField(client.getCNP());
                                                                                             JTextField tf4 = new JTextField(client.getAdress());
                                                                                             tf1.setPreferredSize(new Dimension(80, 24));

                                                                                             tf2.setPreferredSize(new Dimension(80, 24));
                                                                                             tf3.setPreferredSize(new Dimension(80, 24));
                                                                                             tf4.setPreferredSize(new Dimension(80, 24));
                                                                                             JButton update = new JButton("update");
                                                                                             JButton cancel = new JButton("cancel");

                                                                                             p1.add(l1);
                                                                                             p1.add(tf1);
                                                                                             p1.add(l2);
                                                                                             p1.add(tf2);
                                                                                             p1.add(l3);
                                                                                             p1.add(tf3);
                                                                                             p1.add(l4);
                                                                                             p1.add(tf4);
                                                                                             p1.add(update);
                                                                                             p1.add(cancel);
                                                                                             f1.add(p1);
                                                                                             update.addActionListener(new java.awt.event.ActionListener() {
                                                                                                                          @Override
                                                                                                                          public void actionPerformed(java.awt.event.ActionEvent evt) {

                                                                                                                              Client clientt = new Client();
                                                                                                                              clientt.setName(tf1.getText());
                                                                                                                              clientt.setCardNumber(tf2.getText());
                                                                                                                              clientt.setCNP(tf3.getText());
                                                                                                                              clientt.setAdress(tf4.getText());
                                                                                                                              clientt.setIdclient(client.getIdclient());
                                                                                                                              try {
                                                                                                                                  if (valimpl.validateClient(clientt.getName(), clientt.getCardNumber(), clientt.getCNP(), clientt.getAdress())) {
                                                                                                                                      s.update(clientt);
                                                                                                                                      f1.setVisible(false);
                                                                                                                                  } else {
                                                                                                                                      JOptionPane.showMessageDialog(frame,
                                                                                                                                              "Wrong data or empty fields.",
                                                                                                                                              "ERROR",
                                                                                                                                              JOptionPane.ERROR_MESSAGE);
                                                                                                                                  }
                                                                                                                              } catch (ValidationException e) {
                                                                                                                                  e.getErrors().stream().forEach(error -> System.out.println(error));
                                                                                                                              }
                                                                                                                          }
                                                                                                                      }
                                                                                             );
                                                                                             cancel.addActionListener(new java.awt.event.ActionListener() {
                                                                                                                          @Override
                                                                                                                          public void actionPerformed(java.awt.event.ActionEvent evt) {
                                                                                                                              f1.setVisible(false);
                                                                                                                          }
                                                                                                                      }
                                                                                             );
                                                                                         } catch (ValidationException e) {
                                                                                             e.getErrors().stream().forEach(error -> System.out.println(error));
                                                                                         }
                                                                                     }
                                                                                 }
                                                            );
                                                            b3.addActionListener(new java.awt.event.ActionListener() {
                                                                @Override
                                                                public void actionPerformed(java.awt.event.ActionEvent evt) {
                                                                    JButton j4 = new JButton("Cancel");
                                                                    JFrame f1 = new JFrame();
                                                                    f1.setSize(750, 400);
                                                                    f1.setResizable(true);
                                                                    f1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                                                                    f1.setLocationRelativeTo(null);
                                                                    f1.setVisible(true);
                                                                    JPanel p1 = new JPanel();
                                                                    JTable contTable;
                                                                    JScrollPane contScrollPane;

                                                                    DefaultTableModel contTableModel;
                                                                    String[] custColName = {"idclient", "name", "cardNumber", "CNP", "adress"};
                                                                    Object[][] fakeData2 = {};

                                                                    contTableModel = new DefaultTableModel(fakeData2, custColName);
                                                                    contTable = new JTable(contTableModel);
                                                                    contTable.setPreferredScrollableViewportSize(new Dimension(600, 400));
                                                                    contTable.setFillsViewportHeight(true);
                                                                    contTable.setEnabled(false);
                                                                    contScrollPane = new JScrollPane(contTable);

                                                                    try {
                                                                        ResultSet rs = s.view();
                                                                        while (rs.next()) {
                                                                            String name = rs.getString("name");
                                                                            String cardNumber = rs.getString("cardNumber");
                                                                            String CNP = rs.getString("CNP");
                                                                            String adress = rs.getString("adress");
                                                                            int idclient1 = rs.getInt("idclient");
                                                                            contTableModel.addRow(new Object[]{idclient1, name, cardNumber, CNP, adress});
                                                                        }


                                                                    } catch (SQLException e) {
                                                                        e.printStackTrace();
                                                                    } catch (ValidationException e) {
                                                                        e.getErrors().stream().forEach(error -> System.out.println(error));
                                                                    }
                                                                    p1.add(contScrollPane);
                                                                    p1.add(j4);
                                                                    f1.add(p1);
                                                                    j4.addActionListener(new java.awt.event.ActionListener() {
                                                                                             @Override
                                                                                             public void actionPerformed(java.awt.event.ActionEvent evt) {
                                                                                                 f1.setVisible(false);
                                                                                             }
                                                                                         }
                                                                    );
                                                                }
                                                            });
                                                            b4.addActionListener(new java.awt.event.ActionListener() {
                                                                                     @Override
                                                                                     public void actionPerformed(java.awt.event.ActionEvent evt) {
                                                                                         JFrame f1 = new JFrame();
                                                                                         f1.setSize(500, 500);
                                                                                         f1.setResizable(false);
                                                                                         f1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                                                                                         f1.setLocationRelativeTo(null);
                                                                                         f1.setVisible(true);
                                                                                         JPanel p1 = new JPanel();
                                                                                         JLabel l1 = new JLabel("idclient: ");
                                                                                         JLabel l2 = new JLabel("idnumber: ");
                                                                                         JLabel l3 = new JLabel("type: ");
                                                                                         JLabel l4 = new JLabel("money: ");
                                                                                         JTextField tf1 = new JTextField();
                                                                                         JTextField tf2 = new JTextField();
                                                                                         JTextField tf3 = new JTextField();
                                                                                         JTextField tf4 = new JTextField();
                                                                                         tf1.setPreferredSize(new Dimension(80, 24));
                                                                                         tf2.setPreferredSize(new Dimension(80, 24));
                                                                                         tf3.setPreferredSize(new Dimension(80, 24));
                                                                                         tf4.setPreferredSize(new Dimension(80, 24));
                                                                                         JButton add = new JButton("add");
                                                                                         JButton cancel = new JButton("cancel");
                                                                                         p1.add(l1);
                                                                                         p1.add(tf1);
                                                                                         p1.add(l2);
                                                                                         p1.add(tf2);
                                                                                         p1.add(l3);
                                                                                         p1.add(tf3);
                                                                                         p1.add(l4);
                                                                                         p1.add(tf4);
                                                                                         p1.add(add);
                                                                                         p1.add(cancel);
                                                                                         f1.add(p1);
                                                                                         add.addActionListener(new java.awt.event.ActionListener() {
                                                                                                                   @Override
                                                                                                                   public void actionPerformed(java.awt.event.ActionEvent evt) {
                                                                                                                       int idclient;
                                                                                                                       Client client = new Client();
                                                                                                                       Account account = new Account();
                                                                                                                       idclient = Integer.parseInt(tf1.getText());
                                                                                                                       try {
                                                                                                                           client = s.findById(idclient);
                                                                                                                       } catch (ValidationException e) {
                                                                                                                           e.getErrors().stream().forEach(error -> System.out.println(error));
                                                                                                                       }
                                                                                                                       account.setClient(client);
                                                                                                                       account.setIdNumber(tf2.getText());
                                                                                                                       account.setType(tf3.getText());
                                                                                                                       account.setMoney(Double.parseDouble(tf4.getText()));
                                                                                                                       account.setDateCreation(date);

                                                                                                                       try {
                                                                                                                           if (valimpl.validareAccount(account.getIdNumber(), account.getType(), account.getMoney())) {
                                                                                                                               s.addAccount(account);
                                                                                                                               f1.setVisible(false);
                                                                                                                           } else {
                                                                                                                               JOptionPane.showMessageDialog(frame,
                                                                                                                                       "Complete all fields.",
                                                                                                                                       "ERROR",
                                                                                                                                       JOptionPane.ERROR_MESSAGE);
                                                                                                                           }
                                                                                                                       } catch (ValidationException e) {
                                                                                                                           e.getErrors().stream().forEach(error -> System.out.println(error));
                                                                                                                       }
                                                                                                                   }
                                                                                                               }
                                                                                         );
                                                                                         cancel.addActionListener(new java.awt.event.ActionListener() {
                                                                                                                      @Override
                                                                                                                      public void actionPerformed(java.awt.event.ActionEvent evt) {
                                                                                                                          f1.setVisible(false);
                                                                                                                      }
                                                                                                                  }
                                                                                         );
                                                                                     }
                                                                                 }
                                                            );
                                                            b5.addActionListener(new java.awt.event.ActionListener() {
                                                                                     @Override
                                                                                     public void actionPerformed(java.awt.event.ActionEvent evt) {
                                                                                         String idaccounts = JOptionPane.showInputDialog("Introduce-ti id-ul contului pe care doriti sa il updatati ", "");
                                                                                         int idaccount;
                                                                                         idaccount = Integer.parseInt(idaccounts);


                                                                                         try {
                                                                                             Account account = s.findAccById(idaccount);

                                                                                             JFrame f1 = new JFrame();
                                                                                             f1.setSize(500, 500);
                                                                                             f1.setResizable(true);
                                                                                             f1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                                                                                             f1.setLocationRelativeTo(null);
                                                                                             f1.setVisible(true);
                                                                                             JPanel p1 = new JPanel();

                                                                                             JLabel l1 = new JLabel("idNumber: ");
                                                                                             JLabel l2 = new JLabel("type: ");
                                                                                             JLabel l3 = new JLabel("money : ");

                                                                                             JTextField tf1 = new JTextField(account.getIdNumber());

                                                                                             JTextField tf2 = new JTextField(account.getType());
                                                                                             JTextField tf3 = new JTextField(Double.toString(account.getMoney()));
                                                                                             tf1.setPreferredSize(new Dimension(80, 24));
                                                                                             tf2.setPreferredSize(new Dimension(80, 24));
                                                                                             tf3.setPreferredSize(new Dimension(80, 24));
                                                                                             JButton update = new JButton("update");
                                                                                             JButton cancel = new JButton("cancel");

                                                                                             p1.add(l1);
                                                                                             p1.add(tf1);
                                                                                             p1.add(l2);
                                                                                             p1.add(tf2);
                                                                                             p1.add(l3);
                                                                                             p1.add(tf3);

                                                                                             p1.add(update);
                                                                                             p1.add(cancel);
                                                                                             f1.add(p1);
                                                                                             update.addActionListener(new java.awt.event.ActionListener() {
                                                                                                                          @Override
                                                                                                                          public void actionPerformed(java.awt.event.ActionEvent evt) {

                                                                                                                              Account accountt = new Account();
                                                                                                                              accountt.setIdaccount(account.getIdaccount());
                                                                                                                              accountt.setIdNumber(tf1.getText());
                                                                                                                              accountt.setType(tf2.getText());
                                                                                                                              accountt.setMoney(Double.parseDouble(tf3.getText()));
                                                                                                                              try {
                                                                                                                                  if (valimpl.validareAccount(accountt.getIdNumber(), accountt.getType(), accountt.getMoney())) {
                                                                                                                                      s.updateAccount(accountt);
                                                                                                                                      f1.setVisible(false);
                                                                                                                                  } else {
                                                                                                                                      JOptionPane.showMessageDialog(frame,
                                                                                                                                              "Complete all fields.",
                                                                                                                                              "ERROR",
                                                                                                                                              JOptionPane.ERROR_MESSAGE);
                                                                                                                                  }
                                                                                                                              } catch (ValidationException e) {
                                                                                                                                  e.getErrors().stream().forEach(error -> System.out.println(error));
                                                                                                                              }
                                                                                                                          }
                                                                                                                      }
                                                                                             );
                                                                                             cancel.addActionListener(new java.awt.event.ActionListener() {
                                                                                                                          @Override
                                                                                                                          public void actionPerformed(java.awt.event.ActionEvent evt) {
                                                                                                                              f1.setVisible(false);
                                                                                                                          }
                                                                                                                      }
                                                                                             );
                                                                                         } catch (ValidationException e) {
                                                                                             e.getErrors().stream().forEach(error -> System.out.println(error));
                                                                                         }
                                                                                     }
                                                                                 }
                                                            );
                                                            b6.addActionListener(new java.awt.event.ActionListener() {
                                                                                     @Override
                                                                                     public void actionPerformed(java.awt.event.ActionEvent evt) {
                                                                                         String idaccounts = JOptionPane.showInputDialog("Introduce-ti id-ul contului pe care doriti sa il stergeti ", "");
                                                                                         int idaccount;
                                                                                         idaccount = Integer.parseInt(idaccounts);
                                                                                         if (idaccount <= 0) {
                                                                                             JOptionPane.showMessageDialog(frame,
                                                                                                     "Incorrect id number.",
                                                                                                     "ERROR",
                                                                                                     JOptionPane.ERROR_MESSAGE);
                                                                                         } else {

                                                                                             try {
                                                                                                 Account account = s.findAccById(idaccount);
                                                                                                 s.deleteAccount(account);
                                                                                             } catch (ValidationException e) {
                                                                                                 e.getErrors().stream().forEach(error -> System.out.println(error));
                                                                                             }
                                                                                         }
                                                                                     }
                                                                                 }
                                                            );
                                                            b7.addActionListener(new java.awt.event.ActionListener() {
                                                                @Override
                                                                public void actionPerformed(java.awt.event.ActionEvent evt) {
                                                                    JButton j4 = new JButton("Cancel");
                                                                    JFrame f1 = new JFrame();
                                                                    f1.setSize(750, 400);
                                                                    f1.setResizable(true);
                                                                    f1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                                                                    f1.setLocationRelativeTo(null);
                                                                    f1.setVisible(true);
                                                                    JPanel p1 = new JPanel();
                                                                    JTable contTable;
                                                                    JScrollPane contScrollPane;

                                                                    DefaultTableModel contTableModel;
                                                                    String[] custColName = {"idaccount", "idclient", "idNumber", "type", "money", "datecreation"};
                                                                    Object[][] fakeData2 = {};

                                                                    contTableModel = new DefaultTableModel(fakeData2, custColName);
                                                                    contTable = new JTable(contTableModel);
                                                                    contTable.setPreferredScrollableViewportSize(new Dimension(600, 400));
                                                                    contTable.setFillsViewportHeight(true);
                                                                    contTable.setEnabled(false);
                                                                    contScrollPane = new JScrollPane(contTable);

                                                                    try {
                                                                        ResultSet rs = s.viewAccounts();
                                                                        while (rs.next()) {
                                                                            int idaccount = rs.getInt("idaccount");
                                                                            int idclient = rs.getInt("idclient");
                                                                            String idNumber = rs.getString("idNumber");
                                                                            String type = rs.getString("type");
                                                                            double money = rs.getDouble("money");
                                                                            Date date = rs.getDate("datecreation");

                                                                            contTableModel.addRow(new Object[]{idaccount, idclient, idNumber, type, money, date});
                                                                        }


                                                                    } catch (SQLException e) {
                                                                        e.printStackTrace();
                                                                    } catch (ValidationException e) {
                                                                        e.getErrors().stream().forEach(error -> System.out.println(error));
                                                                    }
                                                                    p1.add(contScrollPane);
                                                                    p1.add(j4);
                                                                    f1.add(p1);
                                                                    j4.addActionListener(new java.awt.event.ActionListener() {
                                                                                             @Override
                                                                                             public void actionPerformed(java.awt.event.ActionEvent evt) {
                                                                                                 f1.setVisible(false);
                                                                                             }
                                                                                         }
                                                                    );
                                                                }
                                                            });
                                                            b8.addActionListener(new java.awt.event.ActionListener() {
                                                                                     @Override
                                                                                     public void actionPerformed(java.awt.event.ActionEvent evt) {

                                                                                         try {
                                                                                             JFrame f1 = new JFrame();
                                                                                             f1.setSize(500, 500);
                                                                                             f1.setResizable(false);
                                                                                             f1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                                                                                             f1.setLocationRelativeTo(null);
                                                                                             f1.setVisible(true);
                                                                                             JPanel p1 = new JPanel();

                                                                                             JLabel l1 = new JLabel("Id cont de la care vreti sa transferati: ");
                                                                                             JLabel l2 = new JLabel("Id cont in care vreti sa transferati: ");
                                                                                             JLabel l3 = new JLabel("Suma pe care doriti sa o transferati: ");

                                                                                             JTextField tf1 = new JTextField();
                                                                                             JTextField tf2 = new JTextField();
                                                                                             JTextField tf3 = new JTextField();

                                                                                             tf1.setPreferredSize(new Dimension(80, 24));
                                                                                             tf2.setPreferredSize(new Dimension(80, 24));
                                                                                             tf3.setPreferredSize(new Dimension(80, 24));
                                                                                             JButton update = new JButton("transfer");
                                                                                             JButton cancel = new JButton("cancel");

                                                                                             p1.add(l1);
                                                                                             p1.add(tf1);
                                                                                             p1.add(l2);
                                                                                             p1.add(tf2);
                                                                                             p1.add(l3);
                                                                                             p1.add(tf3);
                                                                                             p1.add(update);
                                                                                             p1.add(cancel);
                                                                                             f1.add(p1);
                                                                                             update.addActionListener(evt1 -> {
                                                                                                 int idaccount = Integer.parseInt(tf1.getText());
                                                                                                 int idaccount2 = Integer.parseInt(tf2.getText());

                                                                                                 try {
                                                                                                     Account a1 = s.findAccById(idaccount);
                                                                                                     Account a2 = s.findAccById(idaccount2);
                                                                                                     double suma = Double.parseDouble(tf3.getText());
                                                                                                     s.updateSuma(a1, a2, suma);
                                                                                                     f1.setVisible(false);

                                                                                                 } catch (ValidationException e) {
                                                                                                     e.getErrors().forEach(error -> System.out.println(error));
                                                                                                 }
                                                                                             }
                                                                                             );
                                                                                             cancel.addActionListener(new java.awt.event.ActionListener() {
                                                                                                                          @Override
                                                                                                                          public void actionPerformed(java.awt.event.ActionEvent evt) {
                                                                                                                              f1.setVisible(false);
                                                                                                                          }
                                                                                                                      }
                                                                                             );
                                                                                         } catch (Exception e) {
                                                                                             e.printStackTrace();
                                                                                         }
                                                                                     }
                                                                                 }
                                                            );
                                                            b9.addActionListener(new java.awt.event.ActionListener() {
                                                                @Override
                                                                public void actionPerformed(java.awt.event.ActionEvent evt) {
                                                                    JButton j4 = new JButton("Cancel");
                                                                    JFrame f1 = new JFrame();
                                                                    f1.setSize(750, 400);
                                                                    f1.setResizable(true);
                                                                    f1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                                                                    f1.setLocationRelativeTo(null);
                                                                    f1.setVisible(true);
                                                                    JPanel p1 = new JPanel();
                                                                    JTable contTable;
                                                                    JScrollPane contScrollPane;

                                                                    DefaultTableModel contTableModel;
                                                                    String[] custColName = {"idbills", "name", "value", "clientid", "status"};
                                                                    Object[][] fakeData2 = {};

                                                                    contTableModel = new DefaultTableModel(fakeData2, custColName);
                                                                    contTable = new JTable(contTableModel);
                                                                    contTable.setPreferredScrollableViewportSize(new Dimension(600, 400));
                                                                    contTable.setFillsViewportHeight(true);
                                                                    contTable.setEnabled(false);
                                                                    contScrollPane = new JScrollPane(contTable);

                                                                    try {
                                                                        ResultSet rs = s.viewBills();
                                                                        while (rs.next()) {
                                                                            int idaccount = rs.getInt("idbills");
                                                                            String idclient = rs.getString("name");
                                                                            double idNumber = rs.getDouble("value");
                                                                            int type = rs.getInt("clientid");
                                                                            String money = rs.getString("status");

                                                                            contTableModel.addRow(new Object[]{idaccount, idclient, idNumber, type, money});
                                                                        }


                                                                    } catch (SQLException e) {
                                                                        e.printStackTrace();
                                                                    } catch (ValidationException e) {
                                                                        e.getErrors().stream().forEach(error -> System.out.println(error));
                                                                    }
                                                                    p1.add(contScrollPane);
                                                                    p1.add(j4);
                                                                    f1.add(p1);
                                                                    j4.addActionListener(new java.awt.event.ActionListener() {
                                                                                             @Override
                                                                                             public void actionPerformed(java.awt.event.ActionEvent evt) {
                                                                                                 f1.setVisible(false);
                                                                                             }
                                                                                         }
                                                                    );
                                                                }
                                                            });
                                                            b10.addActionListener(new java.awt.event.ActionListener() {
                                                                                      @Override
                                                                                      public void actionPerformed(java.awt.event.ActionEvent evt) {

                                                                                          try {
                                                                                              JFrame f1 = new JFrame();
                                                                                              f1.setSize(500, 500);
                                                                                              f1.setResizable(false);
                                                                                              f1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                                                                                              f1.setLocationRelativeTo(null);
                                                                                              f1.setVisible(true);
                                                                                              JPanel p1 = new JPanel();

                                                                                              JLabel l1 = new JLabel("Id factura: ");

                                                                                              JTextField tf1 = new JTextField();
                                                                                              tf1.setPreferredSize(new Dimension(80, 24));
                                                                                              JButton update = new JButton("plateste");
                                                                                              JButton cancel = new JButton("cancel");

                                                                                              p1.add(l1);
                                                                                              p1.add(tf1);
                                                                                              p1.add(update);
                                                                                              p1.add(cancel);
                                                                                              f1.add(p1);
                                                                                              update.addActionListener(new java.awt.event.ActionListener() {
                                                                                                                           @Override
                                                                                                                           public void actionPerformed(java.awt.event.ActionEvent evt) {
                                                                                                                               int idbills = Integer.parseInt(tf1.getText());

                                                                                                                               try {
                                                                                                                                   Bills b = s.findBillById(idbills);
                                                                                                                                   Account a = s.findAccByClientId(b.getClient().getIdclient());
                                                                                                                                   double suma = b.getValue();
                                                                                                                                   s.payBill(b, a, suma);
                                                                                                                                   f1.setVisible(false);

                                                                                                                               } catch (ValidationException e) {
                                                                                                                                   e.getErrors().stream().forEach(error -> System.out.println(error));
                                                                                                                               }
                                                                                                                           }
                                                                                                                       }
                                                                                              );
                                                                                              cancel.addActionListener(new java.awt.event.ActionListener() {
                                                                                                                           @Override
                                                                                                                           public void actionPerformed(java.awt.event.ActionEvent evt) {
                                                                                                                               f1.setVisible(false);
                                                                                                                           }
                                                                                                                       }
                                                                                              );
                                                                                          } catch (Exception e) {
                                                                                              e.printStackTrace();
                                                                                          }
                                                                                      }
                                                                                  }
                                                            );
                                                        } else {
                                                            JOptionPane.showMessageDialog(frame,
                                                                    "Username or password incorrect.",
                                                                    "ERROR",
                                                                    JOptionPane.ERROR_MESSAGE);
                                                            area5.setText("");
                                                            area6.setText("");
                                                        }
                                                    } catch (Exception e) {
                                                        e.printStackTrace();
                                                    }
                                                }
                                            }
                );
                deleteClient.addActionListener(new java.awt.event.ActionListener() {
                                                   @Override
                                                   public void actionPerformed(java.awt.event.ActionEvent evt) {
                                                       x.setVisible(false);
                                                   }
                                               }
                );

            }
        });

        connectAsAdministrator.addActionListener(new java.awt.event.ActionListener() {
            @Override
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                JFrame x = new JFrame();
                JPanel a = new JPanel();
                JLabel label1 = new JLabel("User");
                JTextField area5 = new JTextField();
                area5.setPreferredSize(new Dimension(80, 24));
                a.add(label1);
                a.add(area5);
                a.setLayout(new FlowLayout());
                JPasswordField area6 = new JPasswordField();
                JLabel label2 = new JLabel("Password");
                area6.setPreferredSize(new Dimension(80, 24));
                area6.setEchoChar('*');
                a.add(label2);
                a.add(area6);
                JButton addClient = new JButton("OK");
                JButton deleteClient = new JButton("Cancel");
                a.setLayout(new FlowLayout());
                a.add(addClient);
                a.add(deleteClient);
                x.add(a);
                x.setSize(300, 150);
                x.setResizable(false);
                x.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                x.setLocationRelativeTo(null);
                x.setVisible(true);
                addClient.addActionListener(new java.awt.event.ActionListener() {
                                                @Override
                                                public void actionPerformed(java.awt.event.ActionEvent evt) {
                                                    connectedAsSupplier = dbcon.connectToDatabaseAsSupplier(area5.getText(), area6.getText());
                                                    if (connectedAsSupplier) {
                                                        x.setVisible(false);
                                                        mainFrame.setVisible(false);
                                                        JFrame bbb = new JFrame();
                                                        bbb.setSize(500, 500);
                                                        bbb.setResizable(true);
                                                        bbb.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                                                        bbb.setLocationRelativeTo(null);
                                                        bbb.setVisible(true);
                                                        JPanel p1 = new JPanel();
                                                        JButton b4 = new JButton("Add User");
                                                        JButton b5 = new JButton("Update User");
                                                        JButton b6 = new JButton("Delete User");
                                                        JButton b7 = new JButton("View Users");
                                                        p1.add(b4);
                                                        p1.add(b5);
                                                        p1.add(b6);
                                                        p1.add(b7);
                                                        bbb.add(p1);
                                                        b4.addActionListener(new java.awt.event.ActionListener() {
                                                                                 @Override
                                                                                 public void actionPerformed(java.awt.event.ActionEvent evt) {
                                                                                     JFrame f1 = new JFrame();
                                                                                     f1.setSize(500, 500);
                                                                                     f1.setResizable(false);
                                                                                     f1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                                                                                     f1.setLocationRelativeTo(null);
                                                                                     f1.setVisible(true);
                                                                                     JPanel p1 = new JPanel();
                                                                                     JLabel l1 = new JLabel("Name: ");
                                                                                     JLabel l2 = new JLabel("Password: ");
                                                                                     JLabel l3 = new JLabel("Adress: ");
                                                                                     JLabel l4 = new JLabel("Salary: ");
                                                                                     JTextField tf1 = new JTextField();
                                                                                     JTextField tf2 = new JTextField();
                                                                                     JTextField tf3 = new JTextField();
                                                                                     JTextField tf4 = new JTextField();
                                                                                     tf1.setPreferredSize(new Dimension(80, 24));
                                                                                     tf2.setPreferredSize(new Dimension(80, 24));
                                                                                     tf3.setPreferredSize(new Dimension(80, 24));
                                                                                     tf4.setPreferredSize(new Dimension(80, 24));
                                                                                     JButton add = new JButton("add");
                                                                                     JButton cancel = new JButton("cancel");
                                                                                     p1.add(l1);
                                                                                     p1.add(tf1);
                                                                                     p1.add(l2);
                                                                                     p1.add(tf2);
                                                                                     p1.add(l3);
                                                                                     p1.add(tf3);
                                                                                     p1.add(l4);
                                                                                     p1.add(tf4);
                                                                                     p1.add(add);
                                                                                     p1.add(cancel);
                                                                                     f1.add(p1);
                                                                                     add.addActionListener(new java.awt.event.ActionListener() {
                                                                                                               @Override
                                                                                                               public void actionPerformed(java.awt.event.ActionEvent evt) {
                                                                                                                   User user = new User();
                                                                                                                   user.setName(tf1.getText());
                                                                                                                   user.setPassword(tf2.getText());
                                                                                                                   user.setAdress(tf3.getText());
                                                                                                                   user.setSalary(Double.parseDouble(tf4.getText()));
                                                                                                                   try {
                                                                                                                       if (valimpl.validateUser(user.getName(), user.getPassword(), user.getAdress(), user.getSalary())) {
                                                                                                                           s.addUser(user);
                                                                                                                           f1.setVisible(false);
                                                                                                                       } else {
                                                                                                                           JOptionPane.showMessageDialog(frame,
                                                                                                                                   "Complete all fields.",
                                                                                                                                   "ERROR",
                                                                                                                                   JOptionPane.ERROR_MESSAGE);
                                                                                                                       }
                                                                                                                   } catch (ValidationException e) {
                                                                                                                       e.getErrors().stream().forEach(error -> System.out.println(error));
                                                                                                                   }
                                                                                                               }
                                                                                                           }
                                                                                     );
                                                                                     cancel.addActionListener(new java.awt.event.ActionListener() {
                                                                                                                  @Override
                                                                                                                  public void actionPerformed(java.awt.event.ActionEvent evt) {
                                                                                                                      f1.setVisible(false);
                                                                                                                  }
                                                                                                              }
                                                                                     );
                                                                                 }
                                                                             }
                                                        );
                                                        b5.addActionListener(new java.awt.event.ActionListener() {
                                                                                 @Override
                                                                                 public void actionPerformed(java.awt.event.ActionEvent evt) {
                                                                                     String idusers = JOptionPane.showInputDialog("Introduce-ti id-ul userului pe care doriti sa il updatati ", "");
                                                                                     int iduser;
                                                                                     iduser = Integer.parseInt(idusers);


                                                                                     try {
                                                                                         User user = s.findUserbyId(iduser);

                                                                                         JFrame f1 = new JFrame();
                                                                                         f1.setSize(500, 500);
                                                                                         f1.setResizable(true);
                                                                                         f1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                                                                                         f1.setLocationRelativeTo(null);
                                                                                         f1.setVisible(true);
                                                                                         JPanel p1 = new JPanel();

                                                                                         JLabel l1 = new JLabel("Name: ");
                                                                                         JLabel l2 = new JLabel("Password: ");
                                                                                         JLabel l3 = new JLabel("Address: ");
                                                                                         JLabel l4 = new JLabel("Salary: ");

                                                                                         JTextField tf1 = new JTextField(user.getName());
                                                                                         JTextField tf2 = new JTextField(user.getPassword());
                                                                                         JTextField tf3 = new JTextField(user.getAdress());
                                                                                         JTextField tf4 = new JTextField(Double.toString(user.getSalary()));
                                                                                         tf1.setPreferredSize(new Dimension(80, 24));
                                                                                         tf2.setPreferredSize(new Dimension(80, 24));
                                                                                         tf3.setPreferredSize(new Dimension(80, 24));
                                                                                         tf4.setPreferredSize(new Dimension(80, 24));
                                                                                         JButton update = new JButton("update");
                                                                                         JButton cancel = new JButton("cancel");

                                                                                         p1.add(l1);
                                                                                         p1.add(tf1);
                                                                                         p1.add(l2);
                                                                                         p1.add(tf2);
                                                                                         p1.add(l3);
                                                                                         p1.add(tf3);
                                                                                         p1.add(l4);
                                                                                         p1.add(tf4);

                                                                                         p1.add(update);
                                                                                         p1.add(cancel);
                                                                                         f1.add(p1);
                                                                                         update.addActionListener(new java.awt.event.ActionListener() {
                                                                                                                      @Override
                                                                                                                      public void actionPerformed(java.awt.event.ActionEvent evt) {

                                                                                                                          User userr = new User();
                                                                                                                          userr.setName(tf1.getText());
                                                                                                                          userr.setPassword(tf2.getText());
                                                                                                                          userr.setAdress(tf3.getText());
                                                                                                                          userr.setSalary(Double.parseDouble(tf4.getText()));
                                                                                                                          userr.setIduser(user.getIduser());
                                                                                                                          try {
                                                                                                                              if (valimpl.validateUser(userr.getName(), userr.getPassword(), userr.getAdress(), userr.getSalary())) {
                                                                                                                                  s.updateUser(userr);
                                                                                                                                  f1.setVisible(false);
                                                                                                                              } else {
                                                                                                                                  JOptionPane.showMessageDialog(frame,
                                                                                                                                          "Complete all fields.",
                                                                                                                                          "ERROR",
                                                                                                                                          JOptionPane.ERROR_MESSAGE);
                                                                                                                              }
                                                                                                                          } catch (ValidationException e) {
                                                                                                                              e.getErrors().stream().forEach(error -> System.out.println(error));
                                                                                                                          }
                                                                                                                      }
                                                                                                                  }
                                                                                         );
                                                                                         cancel.addActionListener(new java.awt.event.ActionListener() {
                                                                                                                      @Override
                                                                                                                      public void actionPerformed(java.awt.event.ActionEvent evt) {
                                                                                                                          f1.setVisible(false);
                                                                                                                      }
                                                                                                                  }
                                                                                         );
                                                                                     } catch (ValidationException e) {
                                                                                         e.getErrors().stream().forEach(error -> System.out.println(error));
                                                                                     }
                                                                                 }
                                                                             }
                                                        );
                                                        b6.addActionListener(new java.awt.event.ActionListener() {
                                                                                 @Override
                                                                                 public void actionPerformed(java.awt.event.ActionEvent evt) {
                                                                                     String idusers = JOptionPane.showInputDialog("Introduce-ti id-ul contului pe care doriti sa il stergeti ", "");
                                                                                     int iduser;
                                                                                     iduser = Integer.parseInt(idusers);
                                                                                     if (iduser <= 0) {
                                                                                         JOptionPane.showMessageDialog(frame,
                                                                                                 "Incorrect id number.",
                                                                                                 "ERROR",
                                                                                                 JOptionPane.ERROR_MESSAGE);
                                                                                     } else {

                                                                                         try {
                                                                                             User user = s.findUserbyId(iduser);
                                                                                             s.deleteUser(user);
                                                                                         } catch (Exception e) {
                                                                                             e.printStackTrace();
                                                                                         }
                                                                                     }
                                                                                 }
                                                                             }
                                                        );
                                                        b7.addActionListener(new java.awt.event.ActionListener() {
                                                            @Override
                                                            public void actionPerformed(java.awt.event.ActionEvent evt) {
                                                                JButton j4 = new JButton("Cancel");
                                                                JFrame f1 = new JFrame();
                                                                f1.setSize(750, 400);
                                                                f1.setResizable(true);
                                                                f1.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
                                                                f1.setLocationRelativeTo(null);
                                                                f1.setVisible(true);
                                                                JPanel p1 = new JPanel();
                                                                JTable contTable;
                                                                JScrollPane contScrollPane;

                                                                DefaultTableModel contTableModel;
                                                                String[] custColName = {"iduser", "name", "password", "adress", "salary"};
                                                                Object[][] fakeData2 = {};

                                                                contTableModel = new DefaultTableModel(fakeData2, custColName);
                                                                contTable = new JTable(contTableModel);
                                                                contTable.setPreferredScrollableViewportSize(new Dimension(600, 400));
                                                                contTable.setFillsViewportHeight(true);
                                                                contTable.setEnabled(false);
                                                                contScrollPane = new JScrollPane(contTable);

                                                                try {
                                                                    ResultSet rs = s.viewUser();
                                                                    while (rs.next()) {
                                                                        int iduser = rs.getInt("iduser");
                                                                        String name = rs.getString("name");
                                                                        String password = rs.getString("password");
                                                                        String adress = rs.getString("adress");
                                                                        double salary = rs.getDouble("salary");

                                                                        contTableModel.addRow(new Object[]{iduser, name, password, adress, salary});
                                                                    }


                                                                } catch (SQLException e) {
                                                                    e.printStackTrace();
                                                                } catch (ValidationException e) {
                                                                    e.getErrors().stream().forEach(error -> System.out.println(error));
                                                                }
                                                                p1.add(contScrollPane);
                                                                p1.add(j4);
                                                                f1.add(p1);
                                                                j4.addActionListener(new java.awt.event.ActionListener() {
                                                                                         @Override
                                                                                         public void actionPerformed(java.awt.event.ActionEvent evt) {
                                                                                             f1.setVisible(false);
                                                                                         }
                                                                                     }
                                                                );
                                                            }
                                                        });
                                                    } else {
                                                        JOptionPane.showMessageDialog(frame,
                                                                "Username or password incorrect.",
                                                                "ERROR",
                                                                JOptionPane.ERROR_MESSAGE);
                                                        area5.setText("");
                                                        area6.setText("");
                                                    }

                                                }
                                            }
                );
                deleteClient.addActionListener(new java.awt.event.ActionListener() {
                                                   @Override
                                                   public void actionPerformed(java.awt.event.ActionEvent evt) {
                                                       x.setVisible(false);
                                                   }
                                               }
                );

            }
        });


    }

    public static void main(String[] args) {
        new GUI();
    }

    @Override
    public void actionPerformed(ActionEvent e) {

    }
}
